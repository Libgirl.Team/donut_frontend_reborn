# Donut frontend

Note: This application cannot be compiled alone, it is meant to be used as a submodule of the whole Donut application.
Some modules are shared through Donut's submodule directory structure.
This could have been avoided, but the projects used to be separate repos.
