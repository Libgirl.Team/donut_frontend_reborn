import { IPreferences } from "../types/preferences";

export const DEFAULT_PREFERENCES: IPreferences = {
  models: {
    columns: [
      "modelType.name",
      "version",
      "modelType.algorithm.name",
      "owner.name"
    ],
    efficiencyTypeIDs: [1, 2, 3],
    evaluationTypeIDs: [1]
  }
};
