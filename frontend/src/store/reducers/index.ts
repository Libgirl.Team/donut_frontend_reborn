import { combineReducers } from "redux";
import preferences from "./preferencesReducer";

const rootReducer = combineReducers({
  preferences
});

export type RootState = ReturnType<typeof rootReducer>;
export default rootReducer;
