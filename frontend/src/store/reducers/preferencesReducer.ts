import { IPreferences } from "../../types/preferences";
import { AnyAction } from "redux";
import { DEFAULT_PREFERENCES } from "../defaults";

export enum PreferencesAction {
  Fetch = "FETCH_PREFERENCES",
  Update = "UPDATE_PREFERENCES",
  Reset = "RESET_PREFERENCES"
}

const initialState = { ...DEFAULT_PREFERENCES };

export default (state: IPreferences = initialState, action: AnyAction) => {
  switch (action.type) {
    case PreferencesAction.Update:
    case PreferencesAction.Fetch:
      return action.payload || initialState;
    case PreferencesAction.Reset:
      return initialState;
    default:
      return state;
  }
};
