import React from "react";
import { LIST_USERS } from "../graphql/queries/userQueries";
import { Table } from "semantic-ui-react";
import day from "dayjs";
import GenericTable from "./GenericTable";
import { User } from "shared/types/auth";
import UserHelper from "../helpers/UserHelper";

const Header = () => (
  <>
    <Table.HeaderCell className="id-header">ID</Table.HeaderCell>
    <Table.HeaderCell>Email</Table.HeaderCell>
    <Table.HeaderCell>Name</Table.HeaderCell>
    <Table.HeaderCell>Team</Table.HeaderCell>
    <Table.HeaderCell>Model</Table.HeaderCell>
    <Table.HeaderCell>Deployment</Table.HeaderCell>
    <Table.HeaderCell className="date-header">Date added</Table.HeaderCell>
  </>
);

const Row = (data: User) => (
  <>
    <Table.Cell className="id-value">{data.id}</Table.Cell>
    <Table.Cell>{data.email}</Table.Cell>
    <Table.Cell>{data.name}</Table.Cell>
    <Table.Cell>{UserHelper.numericToLevel(data.tmLevel)}</Table.Cell>
    <Table.Cell>{UserHelper.numericToLevel(data.mmLevel)}</Table.Cell>
    <Table.Cell>{UserHelper.numericToLevel(data.dmLevel)}</Table.Cell>
    <Table.Cell className="date-value">
      {day(data.insertedAt).format("YYYY-MM-DD")}
    </Table.Cell>
  </>
);

export default () => {
  return (
    <GenericTable
      fixed
      typename="User"
      query={LIST_USERS}
      heading="Users"
      renderHeader={Header}
      renderRow={Row}
      dataKey="users"
      singleResourcePath="/users/"
    />
  );
};
