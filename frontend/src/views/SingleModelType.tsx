import React from "react";
import { gql } from "apollo-boost";
import GenericShow from "../components/GenericShow";
import { MODEL_TYPE_DETAILS } from "../graphql/queries/modelTypeQueries";
import ResourceCard from "../components/GenericShow/ResourceCard";

const query = gql`
  ${MODEL_TYPE_DETAILS}
  query($id: ID!) {
    modelType(id: $id) {
      ...ModelTypeDetails
      description
      insertedAt
      updatedAt
      algorithm {
        description
      }
      models {
        id
        version
        modelType {
          name
        }
      }
    }
  }
`;

const SingleModelType = () => {
  return (
    <GenericShow
      resourceType="Model type"
      resourceKey="modelType"
      query={query}
      dependentResources={["models"]}
    >
      {resource => (
        <>
          <h4>Dependencies</h4>
          <ResourceCard resource={resource.algorithm} />
        </>
      )}
    </GenericShow>
  );
};

export default SingleModelType;
