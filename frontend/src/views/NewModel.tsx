import React from "react";
import CreateUpdateForm, {
  IFormFieldRendererProps
} from "../components/CreateUpdateForm";
import { gql } from "apollo-boost";
import { Grid } from "semantic-ui-react";
import InputField from "../components/InputField";
import Select from "../components/Select";
import TextArea from "../components/TextArea";
import { IModelParams } from "../actions/Models";

const FETCH_MODEL_OPTIONS = gql`
  {
    modelTypes {
      key: id
      value: id
      text: name
    }
    datasets {
      key: id
      value: id
      text: name
    }
  }
`;

export const ModelFields = ({
  params,
  handleSelect,
  handleChange,
  errors,
  rawData: data
}: IFormFieldRendererProps) => (
  <>
    <Grid columns={3}>
      <Grid.Row>
        <Grid.Column>
          <Select
            autoFocus
            label="Model type:"
            name="modelTypeId"
            options={data.modelTypes}
            value={params.modelTypeId}
            onChange={handleSelect}
            error={errors.modelTypeId}
          />
        </Grid.Column>
        <Grid.Column>
          <InputField
            label="Major:"
            name="major"
            type="number"
            value={params.major}
            onChange={handleChange}
            min={1}
            step={1}
          />
        </Grid.Column>
        <Grid.Column>
          <InputField
            label="Minor:"
            name="minor"
            type="number"
            value={params.minor}
            onChange={handleChange}
            error={errors.minor}
            min={0}
            step={1}
          />
        </Grid.Column>
      </Grid.Row>
    </Grid>
    <br />
    <Select
      label="Dataset:"
      name="datasetId"
      options={data.datasets}
      value={params.datasetId}
      onChange={handleSelect}
      error={errors.datasetId}
    />
    <InputField
      label="Model URI:"
      placeholder="gs://bucket-name/resource.bin"
      name="modelURI"
      value={params.modelURI}
      onChange={handleChange}
      error={errors.modelURI}
    />
    <TextArea
      label="Description:"
      name="description"
      value={params.description}
      onChange={handleChange}
      error={errors.description}
    />
  </>
);

export const INITIAL_MODEL_PARAMS: IModelParams = {
  description: "",
  major: 1,
  minor: 0,
  modelTypeId: undefined,
  authorId: undefined,
  datasetId: undefined,
  modelURI: ""
};

export default () => {
  return (
    <CreateUpdateForm
      create
      query={FETCH_MODEL_OPTIONS}
      typename="Model"
      initialParams={INITIAL_MODEL_PARAMS}
      children={ModelFields}
    />
  );
};
