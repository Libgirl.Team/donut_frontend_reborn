import React from "react";
import CreateUpdateForm from "../components/CreateUpdateForm";
import { gql } from "apollo-boost";
import { INITIAL_MODEL_PARAMS, ModelFields } from "./NewModel";

const query = gql`
  query($id: ID!) {
    modelTypes {
      key: id
      value: id
      text: name
    }
    datasets {
      key: id
      value: id
      text: name
    }
    model(id: $id) {
      major
      minor
      description
      modelTypeId
      authorId
      datasetId
      modelURI
    }
  }
`;

export default () => {
  return (
    <CreateUpdateForm
      query={query}
      initialParams={INITIAL_MODEL_PARAMS}
      children={ModelFields}
      typename="Model"
    />
  );
};
