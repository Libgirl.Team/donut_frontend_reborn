import React, { useCallback } from "react";
import { Button } from "semantic-ui-react";
import { useHistory } from "react-router";
const Icon = require("../icons/arrow-left.svg").default;

export default () => {
  const history = useHistory();
  const goBack = useCallback(() => {
    history.goBack();
  }, [history]);
  return (
    <div className="error_page">
      <h1 className="error_page__code">501</h1>
      <h2 className="error_page__subtitle">Not Implemented</h2>
      <p className="error_page__explanation">
        The requested feature has not been implemented yet.
      </p>
      <Button color="blue" size="big" onClick={goBack}>
        <Icon /> Go back
      </Button>
    </div>
  );
};
