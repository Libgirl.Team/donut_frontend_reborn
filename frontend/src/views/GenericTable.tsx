import React from "react";
import { useQuery } from "@apollo/react-hooks";
import { Table } from "semantic-ui-react";
import Loader from "../components/Loader";
import PageWrapper from "../components/PageWrapper";
import SectionTitle from "../components/SectionTitle";
import { DocumentNode } from "graphql";
import { useHistory } from "react-router-dom";
import day from "dayjs";
import CreateButton from "../components/CreateButton";
import Permissions from "shared/permissions";
import { useAuthContext } from "../context/AuthContext";

export interface Props {
  query: DocumentNode;
  heading: string;
  typename: string;
  renderHeader?: () => JSX.Element;
  renderRow?: (data: any) => JSX.Element;
  dataKey: string;
  singleResourcePath?: string;
  newResourcePath?: string;
  fixed?: boolean;
}

interface ITableRowProps {
  data: any;
  renderRow: (data: any) => JSX.Element;
  history: any;
  href?: string | null;
}

const TableRow = ({ data, renderRow: Row, history, href }: ITableRowProps) => {
  const onClick = href ? () => history.push(href) : null;
  return (
    <Table.Row onClick={onClick}>
      <Row {...data} />
    </Table.Row>
  );
};

const GenericHeader = () => (
  <>
    <Table.HeaderCell className="id-header">ID</Table.HeaderCell>
    <Table.HeaderCell>Name</Table.HeaderCell>
    <Table.HeaderCell className="owner-header">Created by</Table.HeaderCell>
    <Table.HeaderCell className="date-header">Date added</Table.HeaderCell>
  </>
);

const GenericRow = (data: any) => (
  <>
    <Table.Cell className="id-value">{data.id}</Table.Cell>
    <Table.Cell>{data.name}</Table.Cell>
    <Table.Cell className="owner-value">{data.owner?.name}</Table.Cell>
    <Table.Cell className="date-value">
      {day(data.insertedAt).format("YYYY-MM-DD")}
    </Table.Cell>
  </>
);

export default ({
  query,
  heading,
  renderHeader,
  renderRow,
  singleResourcePath,
  newResourcePath,
  dataKey,
  typename,
  fixed
}: Props) => {
  const { loading, data } = useQuery(query, { fetchPolicy: "no-cache" });
  const history = useHistory();
  const Header = renderHeader || GenericHeader;
  const Row = renderRow || GenericRow;
  const { user } = useAuthContext();
  if (loading || !data) return <Loader />;
  const rows = data[dataKey];
  const buttons =
    newResourcePath && Permissions.canCreate(user, typename) ? (
      <CreateButton to={newResourcePath} />
    ) : null;
  return (
    <PageWrapper>
      <SectionTitle actionButtons={buttons}>{heading}</SectionTitle>
      <Table
        celled
        className={`generic_table ${singleResourcePath ? "hoverable" : ""}`}
        fixed={fixed}
      >
        <Table.Header>
          <Table.Row>
            <Header />
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {rows.map((row: any) => {
            const href = singleResourcePath
              ? `${singleResourcePath}${row.id}`
              : null;
            return (
              <TableRow
                data={row}
                renderRow={Row}
                history={history}
                href={href}
                key={`row-${row.id}`}
              />
            );
          })}
        </Table.Body>
      </Table>
    </PageWrapper>
  );
};
