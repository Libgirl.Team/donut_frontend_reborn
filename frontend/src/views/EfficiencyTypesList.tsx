import React from "react";
import GenericTable from "./GenericTable";
import { Table } from "semantic-ui-react";
import { gql } from "apollo-boost";
import day from "dayjs";

const query = gql`
  {
    efficiencyTypes {
      id
      insertedAt
      name
      unit
    }
  }
`;

const Header = () => (
  <>
    <Table.HeaderCell className="id-header">ID</Table.HeaderCell>
    <Table.HeaderCell>Name</Table.HeaderCell>
    <Table.HeaderCell>Unit</Table.HeaderCell>
    <Table.HeaderCell className="date-header">Date added</Table.HeaderCell>
  </>
);

const Row = (data: any) => (
  <>
    <Table.Cell className="id-value">{data.id}</Table.Cell>
    <Table.Cell>{data.name}</Table.Cell>
    <Table.Cell>{data.unit}</Table.Cell>
    <Table.Cell className="date-value">
      {day(data.insertedAt).format("YYYY-MM-DD")}
    </Table.Cell>
  </>
);

export default () => (
  <GenericTable
    typename="EfficiencyType"
    query={query}
    heading="Efficiency types"
    dataKey="efficiencyTypes"
    renderRow={Row}
    renderHeader={Header}
    singleResourcePath="/efficiency_types/"
    newResourcePath="/efficiency_types/new"
  />
);
