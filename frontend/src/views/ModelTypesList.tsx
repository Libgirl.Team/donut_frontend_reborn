import React from "react";
import { LIST_MODEL_TYPES } from "../graphql/queries/modelTypeQueries";
import { Table } from "semantic-ui-react";
import day from "dayjs";
import GenericTable from "./GenericTable";

const Header = () => (
  <>
    <Table.HeaderCell className="id-header">ID</Table.HeaderCell>
    <Table.HeaderCell>Name</Table.HeaderCell>
    <Table.HeaderCell>Algorithm</Table.HeaderCell>
    <Table.HeaderCell className="owner-header">Created by</Table.HeaderCell>
    <Table.HeaderCell className="date-header">Date added</Table.HeaderCell>
  </>
);

const Row = (data: any) => (
  <>
    <Table.Cell className="id-value">{data.id}</Table.Cell>
    <Table.Cell>{data.name}</Table.Cell>
    <Table.Cell>{data.algorithm?.name}</Table.Cell>
    <Table.Cell className="owner-value">{data.owner?.name}</Table.Cell>
    <Table.Cell className="date-value">
      {day(data.insertedAt).format("YYYY-MM-DD")}
    </Table.Cell>
  </>
);

export default () => {
  return (
    <GenericTable
      fixed
      typename="ModelType"
      query={LIST_MODEL_TYPES}
      newResourcePath="/model_types/new"
      heading="Model Types"
      renderHeader={Header}
      renderRow={Row}
      dataKey="modelTypes"
      singleResourcePath="/model_types/"
    />
  );
};
