import React from "react";
import { LIST_ALGORITHMS } from "../graphql/queries/algorithmQueries";
import GenericTable from "./GenericTable";

export default () => (
  <GenericTable
    query={LIST_ALGORITHMS}
    typename="Algorithm"
    heading="Algorithms"
    dataKey="algorithms"
    singleResourcePath="/algorithms/"
    newResourcePath="/algorithms/new"
  />
);
