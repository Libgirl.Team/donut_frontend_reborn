import React from "react";
import CreateUpdateForm from "../components/CreateUpdateForm";
import { gql } from "apollo-boost";

const query = gql`
  query($id: ID!) {
    algorithm(id: $id) {
      id
      name
      description
    }
  }
`;

export default () => {
  return <CreateUpdateForm query={query} typename="Algorithm" />;
};
