import React from "react";
import { gql } from "apollo-boost";
import GenericShow from "../components/GenericShow";
import ResourceCard from "../components/GenericShow/ResourceCard";

const query = gql`
  query($id: ID!) {
    evaluationType(id: $id) {
      id
      name
      description
      insertedAt
      updatedAt
      dataset {
        id
        name
        description
      }
    }
  }
`;

const SingleEvaluationType = () => {
  return (
    <GenericShow
      resourceType="Evaluation type"
      resourceKey="evaluationType"
      query={query}
    >
      {resource => (
        <>
          <h4>Dependencies</h4>
          <ResourceCard resource={resource.dataset} />
        </>
      )}
    </GenericShow>
  );
};

export default SingleEvaluationType;

