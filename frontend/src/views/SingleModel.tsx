import React from "react";
import { GET_MODEL } from "../graphql/queries/modelQueries";
import EfficiencyEvaluationTable, {
  TableType
} from "../components/EfficiencyEvaluationTable";
import ResourceCard from "../components/GenericShow/ResourceCard";
import { Message } from "semantic-ui-react";
import GenericShow from "../components/GenericShow";
import Permissions from "shared/permissions";
import { useAuthContext } from "../context/AuthContext";

const DependentRecordMessage = () => (
  <Message
    info
    header="Dependent resources"
    content="Efficiencies and evaluations are dependent resources of a model. They will automatically be removed when you delete the model."
  />
);

export default () => {
  return (
    <GenericShow
      resourceType="Model"
      resourceKey="model"
      query={GET_MODEL}
      listPath="/"
    >
      {resource => {
        const { user } = useAuthContext();
        const deleteable = Permissions.can(user, "deleteModel", resource);
        return (
          <>
            <p className="single_model__owner">
              <strong>Model URI:</strong>{" "}
              <a href={resource.modelURI}>{resource.modelURI}</a>
            </p>
            {deleteable ? <DependentRecordMessage /> : null}
            <EfficiencyEvaluationTable
              model={resource}
              type={TableType.Efficiencies}
            />
            <EfficiencyEvaluationTable
              model={resource}
              type={TableType.Evaluations}
            />
            <h4>Dependencies</h4>
            <ResourceCard resource={resource.modelType} />
            <ResourceCard resource={resource.modelType.algorithm} />
            <ResourceCard
              resource={resource.modelType.usage}
              meta={
                <>
                  Input type: {resource.modelType?.usage?.inputType?.name},
                  output type: {resource.modelType?.usage?.outputType?.name}
                </>
              }
            />
          </>
        );
      }}
    </GenericShow>
  );
};
