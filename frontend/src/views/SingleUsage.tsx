import React from "react";
import { gql } from "apollo-boost";
import GenericShow from "../components/GenericShow";
import ResourceCard from "../components/GenericShow/ResourceCard";

const query = gql`
  query($id: ID!) {
    usage(id: $id) {
      id
      name
      owner {
        id
        name
      }
      inputType {
        id
        name
        description
      }
      outputType {
        id
        name
        description
      }
      description
      insertedAt
      updatedAt
      modelTypes {
        id
        name
      }
    }
  }
`;

const SingleUsage = () => {
  return (
    <GenericShow
      resourceType="Usage"
      resourceKey="usage"
      query={query}
      dependentResources={["modelTypes"]}
    >
      {(resource) => (
        <>
          <h4>Dependencies</h4>
          <ResourceCard
            resource={resource.inputType}
            resourceType="Input type"
          />
          <ResourceCard
            resource={resource.outputType}
            resourceType="Output type"
          />
        </>
      )}
    </GenericShow>
  );
};

export default SingleUsage;
