import React, { useEffect, useState } from "react";
import SectionTitle from "../components/SectionTitle";
import { LIST_MODELS } from "../graphql/queries/modelQueries";
import { Table, Button, Icon } from "semantic-ui-react";
import Loader from "../components/Loader";
import { useQuery } from "@apollo/react-hooks";
import Header from "../components/ModelTable/ModelTableHeader";
import Body from "../components/ModelTable/ModelTableBody";
import { useSelector } from "react-redux";
import { RootState } from "../store/reducers";
import Preferences from "../actions/Preferences";
import ModelTablePreferences from "./ModelTablePreferences";
import CreateButton from "../components/CreateButton";

export default () => {
  const preferences = useSelector((s: RootState) => s.preferences);
  const [showPreferences, setShowPreferences] = useState(false);
  useEffect(() => {
    Preferences.fetchPreferences();
  }, []);
  const { evaluationTypeIDs, efficiencyTypeIDs } = preferences.models;
  const { data, loading } = useQuery(LIST_MODELS, {
    variables: { evaluationTypeIDs, efficiencyTypeIDs },
    fetchPolicy: "no-cache"
  });
  if (loading) return <Loader />;
  return (
    <>
      <SectionTitle
        actionButtons={
          <>
            <Button
              icon
              labelPosition="left"
              onClick={() => setShowPreferences(true)}
              color="blue"
              size="small"
            >
              <Icon name="configure" />
              Customize
            </Button>
            <CreateButton to="/models/new" />
          </>
        }
      >
        Models
      </SectionTitle>
      <Table celled fixed className="hoverable generic_table">
        <Header
          preferences={preferences}
          evaluationTypes={data.evaluationTypes}
          efficiencyTypes={data.efficiencyTypes}
        />
        <Body preferences={preferences} data={data} />
      </Table>
      {showPreferences && (
        <ModelTablePreferences handleClose={() => setShowPreferences(false)} />
      )}
    </>
  );
};
