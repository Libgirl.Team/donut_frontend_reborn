import React from "react";
import { LIST_DATA_TYPES } from "../graphql/queries/dataTypeQueries";
import GenericTable from "./GenericTable";

export default () => (
  <GenericTable
    query={LIST_DATA_TYPES}
    typename="DataType"
    heading="Data Types"
    dataKey="dataTypes"
    singleResourcePath="/data_types/"
    newResourcePath="/data_types/new"
  />
);
