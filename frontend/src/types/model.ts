import { ModelType } from "./modelType";
import { User } from "shared/types/auth";
import { Algorithm } from "./algorithm";

type id = number | string;

export interface Dataset {
  id: id;
  name: string;
  datasetURI: string;
  description: string;
  owner: User;
  ownerId: number;
}

export interface EfficiencyType {
  id: string;
  unit: string | null;
  description: string | null;
  name: string;
  insertedAt: string;
  updatedAt: string;
}

export interface EvaluationType {
  id: string;
  unit: string | null;
  description: string | null;
  name: string;
  datasetId: number;
  dataset: Dataset;
  insertedAt: string;
  updatedAt: string;
}

export interface EfficiencyEvaluationType extends EfficiencyType {}

export interface EfficiencyEvaluation {
  id: string;
  value: number;
  type: EfficiencyType | EvaluationType;
  typeId: number;
  modelId: number;
  insertedAt: string;
  updatedAt: string;
}

export interface Model {
  id: number;
  name: string;
  description: string;
  insertedAt: string;
  deactivatedAt: string | null;
  updatedAt: string;
  version: string;
  algorithmId: number;
  algorithm: Algorithm;
  usageName: string;
  evaluations: EfficiencyEvaluation[];
  efficiencies: EfficiencyEvaluation[];
  evaluationMap: { [key: number]: any };
  efficiencyMap: { [key: number]: any };
  modelType: ModelType;
  modelURI: string;
  ownerId: number;
  owner: User;
  authorId: number | null;
  author?: User | null;
}
