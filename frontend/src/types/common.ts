import { ReactChild, ReactChildren } from "react";
import { RouteComponentProps } from "react-router-dom";

export interface WrapperProps {
  children: ReactChild | ReactChild[] | ReactChildren | string | null | number;
}

interface SingleResourceMatchParams {
  id?: string;
}

export interface SingleResourceMatchProps
  extends RouteComponentProps<SingleResourceMatchParams> {}
