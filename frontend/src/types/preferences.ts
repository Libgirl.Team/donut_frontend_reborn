export interface ModelPreferences {
  columns: string[];
  evaluationTypeIDs: number[];
  efficiencyTypeIDs: number[];
}

export interface IPreferences {
  models: ModelPreferences;
}
