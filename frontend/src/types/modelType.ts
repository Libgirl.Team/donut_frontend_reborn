import { Usage } from "./usage";
import { User } from "shared/types/auth";
import { Algorithm } from "./algorithm";

export interface ModelType {
  id: number;
  name: string;
  description: string;
  insertedAt: string;
  updatedAt: string;
  usage: Usage;
  algorithm: Algorithm;
  owner: User;
  __typename: "ModelType";
}
