export interface Algorithm {
  __typename: "Algorithm";
  id: number;
  name: string;
  description: string;
  insertedAt: string;
  updatedAt: string;
}
