import { User } from "shared/types/auth";

export interface DataType {
  id: number;
  name: string;
  description: string;
  insertedAt: string;
  updatedAt: string;
  owner: User;
}
