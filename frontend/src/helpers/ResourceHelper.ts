import snakeCase from "lodash/snakeCase";
import lowerCase from "lodash/lowerCase";

interface Resource {
  __typename: string;
  id: string | number;
}

export default class ResourceHelper {
  static pluralResourcePath(res: Resource) {
    return `/${snakeCase(res.__typename)}s`;
  }

  static singleResourcePath(res: Resource) {
    return `${ResourceHelper.pluralResourcePath(res)}/${res.id}`;
  }

  static editResourcePath(res: Resource) {
    return `${ResourceHelper.singleResourcePath(res)}/edit`;
  }

  static readableResourceName(res: Resource | string) {
    if (typeof res === "string") return lowerCase(res);
    return lowerCase(res.__typename);
  }
}
