const mapping: any = {
  DEFAULT: 0,
  USER: 1,
  MAINTAINER: 2,
  ADMIN: 3,
  SUPER: 4
};

export default class UserHelper {
  static numericToLevel(input: number | any): string | null {
    if (Object.keys(mapping).indexOf(input) !== -1) return input;
    return Object.keys(mapping).find(key => mapping[key] === input) || null;
  }

  static levelToNumeric(input: string | number): number | null {
    if (typeof input === "number") return input;
    return mapping[input] || null;
  }
}
