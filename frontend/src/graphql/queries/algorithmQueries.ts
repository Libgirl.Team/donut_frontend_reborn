import { gql } from "apollo-boost";

export const LIST_ALGORITHMS = gql`
  {
    algorithms {
      id
      name
      owner {
        name
      }
      insertedAt
    }
  }
`;
