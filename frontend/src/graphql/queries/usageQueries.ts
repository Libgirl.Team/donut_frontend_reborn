import { gql } from "apollo-boost";

export const LIST_USAGES = gql`
  {
    usages {
      id
      name
      insertedAt
      owner {
        id
        name
      }
      inputType {
        id
        name
      }
      outputType {
        id
        name
      }
    }
  }
`;
