import { gql } from "apollo-boost";

export const USER_DATA = gql`
  fragment UserData on User {
    id
    email
    name
    mmLevel
    tmLevel
    dmLevel
    picture
  }
`;

export const FETCH_USER_DATA = gql`
  ${USER_DATA}
  {
    user {
      ...UserData
    }
  }
`;

export const LIST_USERS = gql`
  ${USER_DATA}
  {
    users {
      ...UserData
      insertedAt
    }
  }
`;
