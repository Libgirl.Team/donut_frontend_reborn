import { gql } from "apollo-boost";

export const LIST_DATA_TYPES = gql`
  {
    dataTypes {
      id
      name
      owner {
        id
        name
      }
      insertedAt
      description
    }
  }
`;
