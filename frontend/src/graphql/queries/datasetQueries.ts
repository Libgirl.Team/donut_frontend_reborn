import { gql } from "apollo-boost";

export const LIST_DATASETS = gql`
  {
    datasets {
      id
      name
      owner {
        name
      }
      insertedAt
    }
  }
`;
