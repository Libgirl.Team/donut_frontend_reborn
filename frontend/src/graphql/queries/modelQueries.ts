import { gql } from "apollo-boost";

export const MODEL_DETAILS = gql`
  fragment ModelDetails on Model {
    id
    deactivatedAt
    version
    modelType {
      name
      algorithm {
        name
      }
      usage {
        name
      }
    }
    owner {
      id
      name
    }
    evaluationMap
    efficiencyMap
  }
`;

export const LIST_MODELS = gql`
  ${MODEL_DETAILS}
  query listModels($efficiencyIDs: [ID!], $evaluationIDs: [ID!]) {
    efficiencyTypes(ids: $efficiencyIDs) {
      id
      name
    }
    evaluationTypes(ids: $evaluationIDs) {
      id
      name
    }
    models {
      ...ModelDetails
    }
  }
`;

export const EFFICIENCY_EVALUATION_DETAILS = gql`
  fragment EfficiencyEvaluationDetails on EfficiencyEvaluation {
    id
    value
    typeId
    type {
      name
      unit
    }
    insertedAt
  }
`;

export const GET_MODEL = gql`
  ${EFFICIENCY_EVALUATION_DETAILS}
  query getModel($id: ID!) {
    model(id: $id) {
      id
      version
      insertedAt
      updatedAt
      deactivatedAt
      efficiencies {
        ...EfficiencyEvaluationDetails
      }
      evaluations {
        ...EfficiencyEvaluationDetails
      }
      description
      owner {
        id
        name
        email
      }
      author {
        id
        name
        email
      }
      modelType {
        id
        name
        description
        algorithm {
          id
          name
          description
        }
        usage {
          id
          name
          description
          inputType {
            id
            name
          }
          outputType {
            id
            name
          }
        }
      }
      modelURI
    }
  }
`;

export const LIST_EFFICIENCIES_EVALUATIONS = gql`
  {
    efficiencyTypes {
      id
      name
    }
    evaluationTypes {
      id
      name
    }
  }
`;
