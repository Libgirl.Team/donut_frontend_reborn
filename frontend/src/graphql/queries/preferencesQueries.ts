import { gql } from "apollo-boost";

export const FETCH_PREFERENCES = gql`
  {
    user {
      preferences
    }
  }
`;
