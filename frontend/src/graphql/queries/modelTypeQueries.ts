import { gql } from "apollo-boost";

export const MODEL_TYPE_DETAILS = gql`
  fragment ModelTypeDetails on ModelType {
    id
    name
    owner {
      id
      name
    }
    algorithm {
      id
      name
    }
  }
`;

export const LIST_MODEL_TYPES = gql`
  ${MODEL_TYPE_DETAILS}
  query ListModelTypes($ids: [ID!]) {
    modelTypes(ids: $ids) {
      ...ModelTypeDetails
    }
  }
`;

export const GET_MODEL_TYPE = gql`
  ${MODEL_TYPE_DETAILS}
  query getModelType($id: ID!) {
    modelType(id: $id) {
      ...ModelTypeDetails
    }
  }
`;

export const FETCH_MODEL_TYPE_OPTIONS = gql`
  {
    algorithms {
      key: id
      value: id
      text: name
    }
    usages {
      key: id
      value: id
      text: name
    }
  }
`;
