import { gql } from "apollo-boost";

export const UPDATE_PREFERENCES = gql`
  mutation updatePreferences($preferences: PreferencesInput) {
    updatePreferences(preferences: $preferences) {
      success
    }
  }
`;
