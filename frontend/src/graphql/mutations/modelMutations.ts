import { gql } from "apollo-boost";

export const UPDATE_EFFICIENCIES = gql`
  mutation updateEfficiencies(
    $modelId: ID!
    $params: [EfficiencyEvaluationParam!]!
  ) {
    setModelEfficiencies(id: $modelId, efficiencies: $params) {
      success
    }
  }
`;

export const UPDATE_EVALUATIONS = gql`
  mutation updateEvaluations(
    $modelId: ID!
    $params: [EfficiencyEvaluationParam!]!
  ) {
    setModelEvaluations(id: $modelId, evaluations: $params) {
      success
    }
  }
`;
