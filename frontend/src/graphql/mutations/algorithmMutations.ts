import { gql } from "apollo-boost";

export const CREATE_ALGORITHM = gql`
  mutation createAlgorithm($params: AlgorithmParams!) {
    createAlgorithm(params: $params) {
      success
      errors
      resource {
        id
      }
    }
  }
`;
