import ApolloClient from "apollo-boost";

const client = new ApolloClient({
  uri: CONFIG.api_endpoint,
  credentials: "include"
});

export default client;
