import { DocumentNode } from "graphql";
import getInObject from "lodash/get";

export function getMutationKey(mutation: DocumentNode) {
  return getInObject(
    mutation,
    "definitions[0].selectionSet.selections[0].name.value"
  );
}
