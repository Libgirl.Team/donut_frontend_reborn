import axios, { AxiosRequestConfig, AxiosResponse } from "axios";

const instance = axios.create({
  withCredentials: true,
  baseURL: "http://localhost:3000/v1"
});

export type BaseClientResponse = any; //AxiosResponse & { success: boolean };

export default class BaseClient {
  static async request(
    url: string,
    method: "POST" | "GET" | "PATCH" | "PUT" | "DELETE",
    config?: AxiosRequestConfig
  ): Promise<BaseClientResponse> {
    try {
      const res = await instance.request({
        url,
        method,
        ...config
      });
      return {
        ...res,
        success: true
      };
    } catch (e) {
      return {
        success: false,
        error: e
      };
    }
  }

  static async post(url: string, config?: AxiosRequestConfig) {
    return this.request(url, "POST", config);
  }

  static async get(url: string, config?: AxiosRequestConfig) {
    return this.request(url, "GET", config);
  }
}
