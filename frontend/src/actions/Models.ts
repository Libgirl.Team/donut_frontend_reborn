export interface IModelParams {
  major: number;
  minor: number;
  modelTypeId?: number | string;
  authorId?: number | string;
  datasetId?: number | string;
  description: string;
  modelURI: string;
}
