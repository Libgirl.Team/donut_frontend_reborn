import { useQuery } from "@apollo/react-hooks";
import { FETCH_USER_DATA } from "../graphql/queries/userQueries";

export default function useAuth() {
  const { data, loading, error, refetch } = useQuery(FETCH_USER_DATA, {
    fetchPolicy: "no-cache"
  });
  return {
    user: data?.user,
    loading,
    error,
    refetch
  };
}
