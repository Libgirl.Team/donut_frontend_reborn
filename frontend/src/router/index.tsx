import React from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect
} from "react-router-dom";
import Layout from "./Layout";

import AlgorithmsList from "../views/AlgorithmsList";
import DataTypesList from "../views/DataTypesList";
import DatasetsList from "../views/DatasetsList";
import LoginPage from "../views/LoginPage";
import ModelEfficiencies from "../views/ModelEfficiencies";
import ModelEvaluations from "../views/ModelEvaluations";
import ModelTypesList from "../views/ModelTypesList";
import ModelsList from "../views/ModelsList";
import NewAlgorithm from "../views/NewAlgorithm";
import NewDataset from "../views/NewDataset";
import NewModel from "../views/NewModel";
import NewModelType from "../views/NewModelType";
import NewUsage from "../views/NewUsage";
import NotFound from "../views/NotFound";
import SingleModel from "../views/SingleModel";
import SingleModelType from "../views/SingleModelType";
import UsagesList from "../views/UsagesList";
import UsersList from "../views/UsersList";

import useAuth from "../hooks/useAuth";
import AuthContext from "../context/AuthContext";
import NewDataType from "../views/NewDataType";
import EfficiencyTypesList from "../views/EfficiencyTypesList";
import EvaluationTypesList from "../views/EvaluationTypesList";
import SingleAlgorithm from "../views/SingleAlgorithm";
import SingleUsage from "../views/SingleUsage";
import SingleDataset from "../views/SingleDataset";
import SingleDataType from "../views/SingleDataType";
import SingleEvaluationType from "../views/SingleEvaluationType";
import SingleEfficiencyType from "../views/SingleEfficiencyType";
import UpdateAlgorithm from "../views/UpdateAlgorithm";
import UpdateDataset from "../views/UpdateDataset";
import UpdateDataType from "../views/UpdateDataType";
import UpdateUsage from "../views/UpdateUsage";
import UpdateModelType from "../views/UpdateModelType";
import UpdateModel from "../views/UpdateModel";
import NewEfficiencyType from "../views/NewEfficiencyType";
import UpdateEfficiencyType from "../views/UpdateEfficiencyType";
import NewEvaluationType from "../views/NewEvaluationType";
import UpdateEvaluationType from "../views/UpdateEvaluationType";

export default function AppRouter() {
  const { user, loading, error, refetch } = useAuth();

  const PrivateRoute = ({ component: Component, ...rest }: any) => (
    <Route
      {...rest}
      render={props =>
        user && !error ? <Component {...props} /> : <Redirect to="/login" />
      }
    />
  );

  return (
    <AuthContext.Provider value={{ user, refetch }}>
      <Router>
        {loading ? (
          <h1>Loading</h1>
        ) : (
          <Layout>
            <Switch>
              <PrivateRoute path="/" component={ModelsList} exact />
              <PrivateRoute
                path="/algorithms/new"
                component={NewAlgorithm}
                exact
              />
              <PrivateRoute
                path="/algorithms"
                component={AlgorithmsList}
                exact
              />
              <PrivateRoute
                path="/algorithms/:id"
                component={SingleAlgorithm}
                exact
              />
              <PrivateRoute
                path="/algorithms/:id/edit"
                component={UpdateAlgorithm}
                exact
              />
              <PrivateRoute
                path="/efficiency_types"
                component={EfficiencyTypesList}
                exact
              />
              <PrivateRoute
                path="/efficiency_types/new"
                component={NewEfficiencyType}
                exact
              />
              <PrivateRoute
                path="/efficiency_types/:id"
                component={SingleEfficiencyType}
                exact
              />
              <PrivateRoute
                path="/efficiency_types/:id/edit"
                component={UpdateEfficiencyType}
                exact
              />
              <PrivateRoute
                path="/evaluation_types"
                component={EvaluationTypesList}
                exact
              />
              <PrivateRoute
                path="/evaluation_types/new"
                component={NewEvaluationType}
                exact
              />
              <PrivateRoute
                path="/evaluation_types/:id"
                component={SingleEvaluationType}
                exact
              />
              <PrivateRoute
                path="/evaluation_types/:id/edit"
                component={UpdateEvaluationType}
                exact
              />
              <PrivateRoute
                path="/model_types"
                component={ModelTypesList}
                exact
              />
              <PrivateRoute
                path="/model_types/new"
                component={NewModelType}
                exact
              />
              <PrivateRoute
                path="/model_types/:id"
                component={SingleModelType}
                exact
              />
              <PrivateRoute
                path="/model_types/:id/edit"
                component={UpdateModelType}
                exact
              />
              <PrivateRoute path="/datasets" component={DatasetsList} exact />
              <PrivateRoute path="/datasets/new" component={NewDataset} exact />
              <PrivateRoute
                path="/datasets/:id"
                component={SingleDataset}
                exact
              />
              <PrivateRoute
                path="/datasets/:id/edit"
                component={UpdateDataset}
                exact
              />
              <PrivateRoute
                path="/data_types"
                component={DataTypesList}
                exact
              />
              <PrivateRoute
                path="/data_types/new"
                component={NewDataType}
                exact
              />
              <PrivateRoute
                path="/data_types/:id"
                component={SingleDataType}
                exact
              />
              <PrivateRoute
                path="/data_types/:id/edit"
                component={UpdateDataType}
                exact
              />
              <PrivateRoute path="/usages" component={UsagesList} exact />
              <PrivateRoute path="/usages/new" component={NewUsage} exact />
              <PrivateRoute path="/usages/:id" component={SingleUsage} exact />
              <PrivateRoute
                path="/usages/:id/edit"
                component={UpdateUsage}
                exact
              />
              <PrivateRoute path="/users" component={UsersList} exact />
              <PrivateRoute path="/models/new" component={NewModel} exact />
              <PrivateRoute path="/models/:id" component={SingleModel} exact />
              <PrivateRoute
                path="/models/:id/edit"
                component={UpdateModel}
                exact
              />
              <PrivateRoute
                path="/models/:id/efficiencies"
                component={ModelEfficiencies}
                exact
              />
              <PrivateRoute
                path="/models/:id/evaluations"
                component={ModelEvaluations}
                exact
              />
              <Route path="/login" exact component={LoginPage} />
              <PrivateRoute component={NotFound} />
            </Switch>
          </Layout>
        )}
      </Router>
    </AuthContext.Provider>
  );
}
