import React, { useContext } from "react";
import Sidebar from "../components/Sidebar";
import Topbar from "../components/Topbar";
import AuthContext from "../context/AuthContext";
import Breadcrumb from "../components/Breadcrumb";

interface Props {
  children: any;
}

export default ({ children }: Props) => {
  const { user } = useContext(AuthContext);
  if (!user) return <div className="background">{children}</div>;
  return (
    <div className="app">
      <Topbar></Topbar>
      <Sidebar />
      <div className="right-side">
        <main>
          <Breadcrumb />
          {children}
        </main>
        <footer>&copy; 2019-2020 by Libgirl Inc. All rights reserved.</footer>
      </div>
    </div>
  );
};
