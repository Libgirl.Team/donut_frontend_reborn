import { createContext, useContext } from "react";
import { User } from "shared/types/auth";

interface IAuthContextState {
  user: User | null;
  refetch(): void;
}

const initialState: IAuthContextState = {
  user: null,
  refetch: () => null
};

const context = createContext(initialState);

export const useAuthContext = () => {
  const { user } = useContext(context);
  return { user };
};

export default context;
