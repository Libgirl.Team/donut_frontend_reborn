import React from "react";
import { useLocation, NavLink } from "react-router-dom";
import capitalize from "lodash/capitalize";
import lowerCase from "lodash/lowerCase";

const homeSection = { key: "Home", content: "Home", link: true, href: "/" };

const sectionTitle = (sec: string) => {
  if (sec.match(/\d+/)) return `#${sec}`;
  return capitalize(lowerCase(sec));
};

// "/models" should direct to root
const sectionHref = (sections: string[], index: number) => {
  const generated = "/" + sections.slice(0, index + 1).join("/");
  return generated === "/models" ? "/" : generated;
};

const pathToSections = (pathname: string) => {
  const sections = pathname.split("/").filter(Boolean);
  const generated = sections.map((sec, index) => ({
    key: sec,
    content: sectionTitle(sec),
    href: sectionHref(sections, index),
    link: true
  }));
  generated.unshift(homeSection);
  return generated;
};

export default () => {
  const location = useLocation();
  const sections = pathToSections(location?.pathname);
  return (
    <div className="ui breadcrumb">
      {sections.map((sec, index) => (
        <React.Fragment key={`breadcrumb-${sec.key}`}>
          <NavLink to={sec.href}>{sec.content}</NavLink>
          {sections[index + 1] ? (
            <i aria-hidden="true" className="right angle icon divider" />
          ) : null}
        </React.Fragment>
      ))}
    </div>
  );
};
