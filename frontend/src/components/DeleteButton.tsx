import React from "react";
import { Icon, Button } from "semantic-ui-react";

interface Props {
  onClick?(e: any): void;
}

export default ({ onClick }: Props) => (
  <Button
    icon
    labelPosition="left"
    negative
    basic
    onClick={onClick}
    size="tiny"
  >
    <Icon name="trash alternate outline" />
    Delete
  </Button>
);
