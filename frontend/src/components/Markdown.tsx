import unified from "unified";
import parse from "remark-parse";
import remark2react from "remark-react";

interface Props {
  source: string;
}

export default ({ source }: Props) => {
  return unified()
    .use(parse)
    .use(remark2react)
    .processSync(source).contents as any;
};
