import React, { ChangeEvent } from "react";
import { Form } from "semantic-ui-react";

interface Props {
  autoFocus?: boolean;
  error?: string;
  label?: string;
  name: string;
  onChange(e: ChangeEvent<any>): any;
  placeholder?: string;
  type?: string;
  value: any;
  inputLabel?: any;
  min?: string | number;
  max?: string | number;
  step?: string | number;
}

export default function InputField({
  autoFocus,
  name,
  type,
  placeholder,
  onChange,
  value,
  error,
  label,
  inputLabel,
  min,
  max,
  step
}: Props) {
  const input = (
    <input
      autoFocus={autoFocus}
      placeholder={placeholder}
      name={name}
      type={type || "text"}
      value={value}
      onChange={onChange}
      min={min}
      max={max}
      step={step}
    />
  );
  return (
    <Form.Field error={error}>
      {label ? <label>{label}</label> : null}
      {inputLabel ? (
        <div className="ui right labeled input">
          {input}
          <div className="ui basic label">{inputLabel}</div>
        </div>
      ) : (
        input
      )}
      {error ? <span className="error_explanation">{error}</span> : null}
    </Form.Field>
  );
}
