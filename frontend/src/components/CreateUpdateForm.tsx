import React, {
  FormEvent,
  useReducer,
  useCallback,
  Reducer,
  useEffect,
  useMemo,
  ChangeEvent,
  SyntheticEvent
} from "react";
import { useQuery, useMutation } from "@apollo/react-hooks";
import { DocumentNode } from "graphql";
import { useParams } from "react-router";
import Loader from "./Loader";
import { Redirect } from "react-router-dom";
import ResourceHelper from "../helpers/ResourceHelper";
import { Form } from "semantic-ui-react";
import SectionTitle from "./SectionTitle";
import InputField from "./InputField";
import TextArea from "./TextArea";
import { gql } from "apollo-boost";
import SaveButton from "./SaveButton";
import { getMutationKey } from "../graphql/helpers";
import camelCase from "lodash/camelCase";
import ErrorCallout from "./ErrorCallout";

export interface IFormFieldRendererProps {
  handleChange(e: ChangeEvent<any>): void;
  handleSelect(e: SyntheticEvent, data: any): void;
  params: { [k: string]: any };
  errors: { [k: string]: any };
  rawData: any;
}

interface Props {
  create?: boolean;
  query?: DocumentNode;
  initialParams?: { [k: string]: any };
  children?(props: IFormFieldRendererProps): React.ReactNode;
  typename: string;
}

enum ReducerEventType {
  SetParam,
  DataLoaded
}

interface ReducerEvent {
  type: ReducerEventType;
  payload?: any;
}

const defaultInitialParams = {
  name: "",
  description: ""
};

const generateMutation = (create: boolean, typename: string) => {
  const str = `
  mutation(${create ? "" : "$id: ID!,"} $params: ${typename}Params!)  {
    ${create ? "create" : "update"}${typename}(${
    create ? "" : "id: $id, "
  }params: $params) {
      success errors resource { id }
    }
  }
  `;
  return gql`
    ${str}
  `;
};

const DefaultFields = ({
  handleChange,
  errors,
  params
}: IFormFieldRendererProps) => (
  <>
    <InputField
      name="name"
      label="Name:"
      value={params.name}
      error={errors.name}
      onChange={handleChange}
    />
    <TextArea
      name="description"
      label="Description:"
      onChange={handleChange}
      error={errors.description}
      value={params.description}
    />
  </>
);

const paramReducer: Reducer<any, ReducerEvent> = (
  state: any,
  { type, payload }: ReducerEvent
) => {
  switch (type) {
    case ReducerEventType.DataLoaded:
      return Object.keys(state).reduce((acc: any, key) => {
        acc[key] = payload[key];
        return acc;
      }, {});
    case ReducerEventType.SetParam:
      const { field, value } = payload;
      return {
        ...state,
        [field]: value
      };
  }
};

const NO_OP_QUERY = gql`
  {
    hello
  }
`;

const CreateUpdateForm = ({
  create,
  query,
  initialParams,
  children,
  typename
}: Props) => {
  const { id } = useParams();
  const { data, loading } = useQuery(query || NO_OP_QUERY, {
    variables: { id },
    fetchPolicy: "no-cache"
  });
  const mutation = useMemo(() => generateMutation(!!create, typename), [
    typename,
    create
  ]);
  const mutationKey = useMemo(() => getMutationKey(mutation), [mutation]);
  const [mutate, { called, data: mutationResponse }] = useMutation(mutation);
  const [params, dispatch] = useReducer(
    paramReducer,
    initialParams || defaultInitialParams
  );
  const resourceKey = camelCase(typename);
  const resource = create ? {} : data?.[resourceKey];
  const resourceType = ResourceHelper.readableResourceName(typename);

  useEffect(() => {
    if (!loading && !create)
      dispatch({
        type: ReducerEventType.DataLoaded,
        payload: resource
      });
  }, [loading]);

  const handleSelect = useCallback(
    (_event: any, { name, value }: any) => {
      dispatch({
        type: ReducerEventType.SetParam,
        payload: { field: name, value }
      });
    },
    [dispatch]
  );

  const handleChange = useCallback(
    (e: ChangeEvent<HTMLInputElement> | ChangeEvent<HTMLTextAreaElement>) => {
      const { name, value } = e.target;
      dispatch({
        type: ReducerEventType.SetParam,
        payload: { field: name, value }
      });
    },
    [dispatch]
  );

  if (loading) return <Loader />;
  const result = mutationResponse?.[mutationKey];
  const mutationError = result?.errors?.msg;
  if (called && result?.success)
    return (
      <Redirect to={ResourceHelper.singleResourcePath(result?.resource)} />
    );
  const handleSubmit = (e: FormEvent) => {
    e.preventDefault();
    return mutate({ variables: { params, id } });
  };
  const renderForm = children || DefaultFields;

  return (
    <>
      <ErrorCallout error={mutationError} />
      <Form onSubmit={handleSubmit}>
        <SectionTitle>
          {create ? (
            `Create ${resourceType}`
          ) : (
            <>
              Edit {resourceType} #{resource.id}
            </>
          )}
        </SectionTitle>
        {renderForm({
          handleSelect,
          rawData: data,
          handleChange,
          params,
          errors: result?.errors || {}
        })}
        <SaveButton />
      </Form>
    </>
  );
};

export default CreateUpdateForm;
