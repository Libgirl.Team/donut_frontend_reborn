import React from "react";
import { Link } from "react-router-dom";
import { Icon } from "semantic-ui-react";

interface Props {
  to: string;
}

export default ({ to }: Props) => (
  <Link to={to} className="ui icon lef labeled button positive small">
    <Icon name="plus" />
    New
  </Link>
);
