import React, { ChangeEvent, SyntheticEvent } from "react";
import { Form, Dropdown } from "semantic-ui-react";

export interface ISelectOption {
  key: number | string;
  value: number | string;
  text: string;
}

interface Props {
  error?: string;
  label?: string;
  name: string;
  onChange(e: SyntheticEvent, data: any): any;
  options: ISelectOption[];
  placeholder?: string;
  value: any;
  autoFocus?: boolean;
}

const DEFAULT_PLACEHOLDER = "Select one";

export default function Select({
  autoFocus,
  error,
  label,
  name,
  onChange,
  options,
  placeholder,
  value
}: Props) {
  return (
    <Form.Field error={error}>
      {label ? <label>{label}</label> : null}
      <Dropdown
        autoFocus={autoFocus}
        name={name}
        placeholder={placeholder || DEFAULT_PLACEHOLDER}
        value={value}
        onChange={onChange}
        options={options}
        selection
      />
      {error ? <span className="error_explanation">{error}</span> : null}
    </Form.Field>
  );
}
