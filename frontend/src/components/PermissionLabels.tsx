import React from "react";
import { User, ManagementLevel } from "shared/types/auth";
import UserHelper from "../helpers/UserHelper";

interface Props {
  level: ManagementLevel;
  text: string;
  type: "mm" | "tm" | "dm";
}

const PermissionLevelLabel = ({ level, text, type }: Props) => {
  const numericLevel = UserHelper.levelToNumeric(level);
  const stringLevel = UserHelper.numericToLevel(level);
  const className = `permission_level_label permission_level_label--${type} permission_level_label--level-${numericLevel}`;
  return (
    <div
      className={className}
      title={`${text} management level: ${stringLevel}`}
    >
      {text}
      <span className="permission_level_label__level">{numericLevel}</span>
    </div>
  );
};

export default ({ user }: { user: User }) => (
  <div className="user_permissions">
    <PermissionLevelLabel level={user.tmLevel} text="Team" type="tm" />
    <PermissionLevelLabel level={user.mmLevel} text="Model" type="mm" />
    <PermissionLevelLabel level={user.dmLevel} text="Deployment" type="dm" />
  </div>
);
