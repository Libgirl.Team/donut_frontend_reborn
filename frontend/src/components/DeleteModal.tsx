import React, { useCallback, useState, ChangeEvent, useMemo } from "react";
import Modal from "./Modal";
import { useMutation } from "@apollo/react-hooks";
import InputField from "./InputField";
import { Form } from "semantic-ui-react";
import { useHistory } from "react-router";
import ResourceHelper from "../helpers/ResourceHelper";
import { getMutationKey } from "../graphql/helpers";
import { gql } from "apollo-boost";

interface Props {
  resource: any;
  dependentResources?: string[];
  handleClose(e?: any): void;
  listPath?: string;
}

const CONFIRMATION_TEXT = "the void gazes at you";

const compareInput = (actual: string, expected = CONFIRMATION_TEXT) => {
  return actual.toLowerCase() === expected.toLowerCase();
};

const generateDeleteMutation = (typename: any) => {
  const str = `mutation($id: ID!) { delete${typename}(id: $id) { success errors } }`;
  return gql`
    ${str}
  `;
};

const DeleteModal = ({ resource, listPath, handleClose }: Props) => {
  const [input, setInput] = useState("");
  const mutation = useMemo(() => generateDeleteMutation(resource.__typename), [
    resource.__typename
  ]);
  const mutationKey = useMemo(() => getMutationKey(mutation), [mutation]);
  const [mutate] = useMutation(mutation);
  const history = useHistory();

  const handleSubmit = useCallback(async () => {
    const res = await mutate({ variables: { id: resource.id } });
    const success = res?.data?.[mutationKey]?.success;
    if (success)
      history.push(listPath || ResourceHelper.pluralResourcePath(resource));
  }, [mutate, resource, history, mutationKey]);

  const handleChange = useCallback(
    (e: ChangeEvent<HTMLInputElement>) => {
      setInput(e.target.value);
    },
    [setInput]
  );

  const disabled = !compareInput(input);

  return (
    <Modal
      icon="trash"
      submitLabel="Delete"
      negative
      disabled={disabled}
      handleClose={handleClose}
      title={"Attention! This is a matter of grave importance!"}
      handleSubmit={handleSubmit}
    >
      <p>
        You are about to delete a resource. This action cannot be undone. All
        dependent resources will be removed, together with their respective
        dependent resources.
      </p>
      <p>This may cause cataclysmic changes to the space-time continuum.</p>
      <p>
        In order to proceed, please type <em>{CONFIRMATION_TEXT}</em> in the
        field below.
      </p>
      <Form onSubmit={handleSubmit} disabled={disabled}>
        <InputField
          autoFocus
          name="confirmation"
          placeholder={CONFIRMATION_TEXT}
          value={input}
          onChange={handleChange}
        />
      </Form>
    </Modal>
  );
};

export default DeleteModal;
