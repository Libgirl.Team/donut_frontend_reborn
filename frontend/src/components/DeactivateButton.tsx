import React from "react";
import { Icon } from "semantic-ui-react";

interface Props {
  active: boolean;
  onClick(e: any): void;
}

export default ({ active, onClick }: Props) => (
  <button
    type="button"
    onClick={onClick}
    className={`ui icon left labeled button tiny basic ${
      active ? "negative" : "blue"
    }`}
  >
    <Icon name={active ? "eye slash" : "eye"} />
    {active ? "Deactivate" : "Activate"}
  </button>
);
