import React, { ChangeEvent } from "react";
import { Form } from "semantic-ui-react";

interface Props {
  error?: string;
  label: string;
  name: string;
  onChange(e: ChangeEvent<HTMLTextAreaElement>): any;
  placeholder?: string;
  value: any;
  autoFocus?: boolean;
}

export default function TextArea({
  error,
  label,
  name,
  onChange,
  placeholder,
  value,
  autoFocus
}: Props) {
  return (
    <Form.Field error={error}>
      <label>{label}</label>
      <textarea
        autoFocus={autoFocus}
        name={name}
        onChange={onChange}
        placeholder={placeholder}
        value={value}
      />
      {error ? <span className="error_explanation">{error}</span> : null}
    </Form.Field>
  );
}
