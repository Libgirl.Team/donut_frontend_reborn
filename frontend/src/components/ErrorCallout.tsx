import React from "react";
import { Message } from "semantic-ui-react";

interface Props {
  error: string | null | undefined;
}

export default ({ error }: Props) => {
  if (!error) return null;
  return <Message negative header="An error occurred" content={error} />;
};
