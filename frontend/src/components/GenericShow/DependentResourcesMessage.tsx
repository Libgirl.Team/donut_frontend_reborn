import React from "react";
import { Message } from "semantic-ui-react";

export default () => (
  <Message
    warning
    header="This resource has dependent resources"
    content="When you delete this resource, all dependent resources will be removed, together with their respective dependent resources."
  />
);
