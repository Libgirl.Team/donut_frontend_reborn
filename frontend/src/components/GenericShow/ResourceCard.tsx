import React, { useMemo } from "react";
import { Card } from "semantic-ui-react";
import { Link } from "react-router-dom";
import snakeCase from "lodash/snakeCase";
import lowerCase from "lodash/lowerCase";
import capitalize from "lodash/capitalize";
import MD from "../Markdown";
import ResourceHelper from "../../helpers/ResourceHelper";

interface Props {
  resource: {
    __typename: string;
    id: number | string;
    name: string;
    description: string;
  };
  meta?: React.ReactNode;
  resourceType?: string;
}

const ResourceCard = ({ resource, meta, resourceType }: Props) => {
  const { __typename, id, name, description } = resource;
  const url = ResourceHelper.singleResourcePath(resource);
  const heading = useMemo(
    () => resourceType || capitalize(lowerCase(__typename)),
    [__typename, resourceType]
  );

  return (
    <Card fluid>
      <Card.Content>
        <Card.Header>
          {heading}:{" "}
          <Link to={url}>
            {name} (#{id})
          </Link>
        </Card.Header>

        {meta ? <Card.Meta>{meta}</Card.Meta> : null}
        <Card.Description>
          <MD source={description} />
        </Card.Description>
      </Card.Content>
    </Card>
  );
};

export default ResourceCard;
