import React, { useMemo, useState, useCallback } from "react";
import SectionTitle from "../SectionTitle";
import Container from "../Container";
import { Link } from "react-router-dom";
import { User } from "../../../../shared/types/auth";
import { DocumentNode } from "graphql";
import { useQuery } from "@apollo/react-hooks";
import Loader from "../Loader";
import { useParams } from "react-router";
import day from "dayjs";
import snakeCase from "lodash/snakeCase";
import ResourceHelper from "../../helpers/ResourceHelper";
import MD from "../Markdown";
import DeleteButton from "../DeleteButton";
import EditButton from "../EditButton";
import DeleteModal from "../DeleteModal";
import UserLink from "./UserLink";
import DependentResourcesMessage from "./DependentResourcesMessage";
import Permissions from "shared/permissions";
import { useAuthContext } from "../../context/AuthContext";
import DeactivateButton from "../DeactivateButton";
import DeactivateModal from "../DeactivationModal";

interface Resource {
  __typename: string;
  id: number | string;
  name?: string;
  description?: string;
  insertedAt: string;
  updatedAt: string;
  owner: User;
  author?: User;
}

interface Props {
  resourceType: string;
  query: DocumentNode;
  resourceKey: string;
  dependentResources?: string[];
  children?(resource: any): any;
  listPath?: string;
}

const DependentResource = ({ resource }: any) => {
  if (!resource) return null;
  switch (resource.__typename) {
    case "Model":
      return (
        <p>
          <Link to={`/models/${resource.id}`}>
            Model: {resource?.modelType?.name} v. {resource.version} (#
            {resource.id})
          </Link>
        </p>
      );
    default:
      return (
        <p>
          <Link to={`/${snakeCase(resource.__typename)}s/${resource.id}`}>
            {resource.__typename}: {resource.name} (#{resource.id})
          </Link>
        </p>
      );
  }
};

const GenericShow = ({
  resourceType,
  resourceKey,
  query,
  children,
  dependentResources,
  listPath
}: Props) => {
  const { id } = useParams();
  const { data, loading, refetch } = useQuery(query, {
    variables: { id },
    fetchPolicy: "no-cache"
  });
  const [showDeleteModal, setShowDeleteModal] = useState(false);
  const [showDeactivateModal, setShowDeactivateModal] = useState(false);
  const { user } = useAuthContext();

  const toggleDeleteModal = useCallback(() => {
    setShowDeleteModal(s => !s);
  }, [setShowDeleteModal]);

  const toggleDeactivateModal = useCallback(() => {
    setShowDeactivateModal(s => !s);
  }, [setShowDeactivateModal]);

  const resource = data?.[resourceKey] as Resource;
  const hasDependent = useMemo(
    () =>
      resource &&
      dependentResources?.some(key => (resource as any)[key]?.length),
    [loading]
  );
  if (loading) return <Loader />;
  const updateable = Permissions.canUpdate(user, resource);
  const deleteable = Permissions.canDelete(user, resource);
  const deactivatable =
    resource.__typename == "Model" && Permissions.canDeactivate(user, resource);
  const active = deactivatable && !(resource as any).deactivatedAt;
  const actionButtons = (
    <>
      {updateable ? (
        <EditButton to={ResourceHelper.editResourcePath(resource)} />
      ) : null}
      {deleteable ? <DeleteButton onClick={toggleDeleteModal} /> : null}
      {deactivatable ? (
        <DeactivateButton onClick={toggleDeactivateModal} active={active} />
      ) : null}
    </>
  );
  return (
    <>
      <Container className="generic_show">
        <SectionTitle actionButtons={actionButtons} vertical>
          {resourceType}:{" "}
          {resource.name ? (
            <>
              {resource.name} (#{resource.id})
            </>
          ) : (
            <>(#{resource.id})</>
          )}
        </SectionTitle>
        <UserLink
          user={resource.owner}
          label="Owner"
          className="resource__owner"
        />
        <UserLink
          user={resource.author}
          label="Author"
          className="resource__author"
        />
        <p className="resource__inserted_at">
          <strong>Created at:</strong>{" "}
          {day(resource.insertedAt).format("YYYY-MM-DD HH:mm:ss (UTCZ)")}
        </p>
        <p className="resource__update_at">
          <strong>Updated at:</strong>{" "}
          {day(resource.updatedAt).format("YYYY-MM-DD HH:mm:ss (UTCZ)")}
        </p>
        {resource.description ? (
          <>
            <h4>Description</h4>
            <div className="resource__description">
              <MD source={resource.description} />
            </div>
          </>
        ) : null}
        {children ? children(resource) : (null as any)}
        {hasDependent ? (
          <>
            {deleteable ? <DependentResourcesMessage /> : null}
            <h4>Dependent resources</h4>
            {dependentResources?.map(key =>
              (resource as any)[key].map((r: any) => (
                <DependentResource resource={r} />
              ))
            )}
          </>
        ) : null}
      </Container>
      {deleteable && showDeleteModal ? (
        <DeleteModal
          dependentResources={dependentResources}
          resource={resource}
          handleClose={toggleDeleteModal}
          listPath={listPath}
        />
      ) : null}
      {deactivatable && showDeactivateModal ? (
        <DeactivateModal
          resource={resource}
          handleClose={toggleDeactivateModal}
          handleSuccess={refetch}
        />
      ) : null}
    </>
  );
};

export default GenericShow;
