import React from "react";
import { Link } from "react-router-dom";
import { User } from "shared/types/auth";

interface Props {
  user?: User;
  label: string;
  className: string;
}

export default ({ user, label, className }: Props) => {
  if (typeof user === "undefined") return null;
  return (
    <p className={className}>
      <strong>{label}: </strong>
      {user?.id && user?.name ? (
        <Link to={`/users/${(user as User).id}`}>{(user as User).name}</Link>
      ) : (
        "Unknown"
      )}
    </p>
  );
};
