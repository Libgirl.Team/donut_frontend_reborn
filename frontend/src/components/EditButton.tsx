import React from "react";
import { Link } from "react-router-dom";
import { Icon } from "semantic-ui-react";

interface Props {
  to: string;
  className?: string;
}

export default ({ className, to }: Props) => (
  <Link
    to={to}
    className={`ui icon left labeled button tiny basic ${className}`}
  >
    <Icon name="edit" />
    Edit
  </Link>
);
