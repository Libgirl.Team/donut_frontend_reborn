import React from "react";
import { Table } from "semantic-ui-react";
import { EvaluationType, EfficiencyType } from "../../types/model";
import { IPreferences } from "../../types/preferences";
import { MODEL_COLUMN_HEADINGS } from "../../actions/Preferences";

interface HeaderProps {
  preferences: IPreferences;
  evaluationTypes: EvaluationType[];
  efficiencyTypes: EfficiencyType[];
}

const ModelTableHeader = ({
  preferences,
  evaluationTypes,
  efficiencyTypes
}: HeaderProps) => {
  const { columns, efficiencyTypeIDs, evaluationTypeIDs } =
    preferences.models || {};

  return (
    <Table.Header>
      <Table.Row>
        {columns.map((column: string) => (
          <Table.HeaderCell key={column}>
            {MODEL_COLUMN_HEADINGS[column] || column}
          </Table.HeaderCell>
        ))}
        {evaluationTypeIDs.map(id => {
          const evaluationType = evaluationTypes.find(
            e => String(e.id) === String(id)
          );
          return (
            <Table.HeaderCell
              key={`evaluation-${id}`}
              className="evaluation-header"
            >
              {evaluationType?.name}
            </Table.HeaderCell>
          );
        })}
        {efficiencyTypeIDs.map(id => {
          const efficiencyType = efficiencyTypes.find(
            e => String(e.id) === String(id)
          );
          return (
            <Table.HeaderCell
              key={`efficiency-${id}`}
              className="efficiency-header"
            >
              {efficiencyType?.name}
            </Table.HeaderCell>
          );
        })}
      </Table.Row>
    </Table.Header>
  );
};

export default ModelTableHeader;
