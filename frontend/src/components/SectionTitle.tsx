import React from "react";
import { WrapperProps } from "../types/common";

type Props = WrapperProps & {
  actionButtons?: JSX.Element | null;
  vertical?: boolean;
};

export default ({ children, actionButtons, vertical }: Props) => {
  return (
    <div className="section_title">
      <h1>{children}</h1>
      {actionButtons ? (
        <div
          className={`section_title__actions ${
            vertical ? "section_title__actions--vertical" : ""
          }`}
        >
          {actionButtons}
        </div>
      ) : null}
    </div>
  );
};
