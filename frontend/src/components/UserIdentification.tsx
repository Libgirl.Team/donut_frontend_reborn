import React, { useContext } from "react";
import { User } from "shared/types/auth";
import PermissionLabels from "./PermissionLabels";

interface Props {
  user: User;
}

const UserAvatar = ({ user }: Props) => {
  const initials = user.name
    .split(" ")
    .map(n => n[0])
    .join("");
  return (
    <div className="avatar">
      {user.picture ? (
        <img src={user.picture} className="avatar__img" alt={initials} />
      ) : (
        initials
      )}
    </div>
  );
};

export default ({ user }: Props) => {
  return (
    <>
      <UserAvatar user={user} />
      <div className="user_section__top">
        <span className="user_section__name">{user.name}</span>
        <PermissionLabels user={user} />
      </div>
    </>
  );
};
