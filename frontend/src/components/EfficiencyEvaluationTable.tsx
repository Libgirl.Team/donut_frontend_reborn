import React from "react";
import { Model } from "../types/model";
import { Table } from "semantic-ui-react";
import { Link } from "react-router-dom";
import capitalize from "lodash/capitalize";
import EditButton from "./EditButton";
const EditIcon = require("../icons/edit.svg").default;

export enum TableType {
  Efficiencies = "efficiencies",
  Evaluations = "evaluations"
}

interface Props {
  model: Model;
  type: TableType;
}

const Header = ({ type, model }: Props) => (
  <h4>
    {capitalize(type)}
    <EditButton
      to={`/models/${model.id}/${type}`}
      className="efficiency-evaluation-table__edit"
    />
  </h4>
);

const BlankSlate = ({ type, model }: Props) => (
  <>
    <Header type={type} model={model} />
    <p className="efficiency-evaluation-table__blank-slate">
      No records found. You can add some on the{" "}
      <Link to={`/models/${model.id}/${type}`}>edit page</Link>.
    </p>
  </>
);

export default function EfficiencyEvaluationTable({ model, type }: Props) {
  const data = model[type];
  if (!data.length) return <BlankSlate model={model} type={type} />;
  return (
    <>
      <Header type={type} model={model} />
      <Table celled className={`${type}_table`} fixed>
        <Table.Header>
          <Table.Row>
            {data.map(e => (
              <Table.HeaderCell key={`${type}-header-${e.type?.name}`}>
                {e.type?.name}
              </Table.HeaderCell>
            ))}
          </Table.Row>
        </Table.Header>
        <Table.Body>
          <Table.Row>
            {data.map(e => (
              <Table.Cell key={`${type}-cell-${e.typeId}`}>
                {e.value} {e.type.unit}
              </Table.Cell>
            ))}
          </Table.Row>
        </Table.Body>
      </Table>
    </>
  );
}
