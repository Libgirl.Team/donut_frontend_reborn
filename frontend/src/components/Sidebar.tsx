import React from "react";
import { Link } from "react-router-dom";
const BrainIcon = require("../icons/brain.svg").default;
const ModelTypeIcon = require("../icons/project-diagram.svg").default;
const EvaluationIcon = require("../icons/person-booth.svg").default;
const EfficiencyIcon = require("../icons/clock.svg").default;
const LaptopIcon = require("../icons/laptop-code.svg").default;
const ServerIcon = require("../icons/server.svg").default;
const UsersIcon = require("../icons/users.svg").default;
const PuzzleIcon = require("../icons/puzzle-piece.svg").default;
const WaveIcon = require("../icons/wave-square.svg").default;

interface Props {
  to: string;
  icon: typeof BrainIcon;
  text: string;
}

const SidebarLink = ({ to, icon: Icon, text }: Props) => (
  <Link to={to} className="item sidebar_link">
    <Icon />
    {text}
  </Link>
);

export default () => {
  return (
    <aside className="sidebar">
      <SidebarLink to="/" text="Models" icon={BrainIcon} />
      <SidebarLink to="/model_types" text="Model types" icon={ModelTypeIcon} />
      <SidebarLink to="/algorithms" text="Algorithms" icon={LaptopIcon} />
      <SidebarLink to="/users" text="Users" icon={UsersIcon} />
      <SidebarLink to="/datasets" text="Datasets" icon={ServerIcon} />
      <SidebarLink to="/data_types" text="Data types" icon={WaveIcon} />
      <SidebarLink to="/usages" text="Usages" icon={PuzzleIcon} />
      <SidebarLink
        to="/efficiency_types"
        text="Efficiency types"
        icon={EfficiencyIcon}
      />
      <SidebarLink
        to="/evaluation_types"
        text="Evaluation types"
        icon={EvaluationIcon}
      />
    </aside>
  );
};
