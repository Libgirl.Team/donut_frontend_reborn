#!/bin/sh

set -e

export NODE_ENV=production

cd /usr/local/lib/donut/frontend
git pull
npm install
npm run build
echo "Done"
exit 0
